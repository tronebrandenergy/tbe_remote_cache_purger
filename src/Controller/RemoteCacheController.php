<?php

/**
 * @file
 * Contains Drupal\tbe_remote_cache_purger\Controller\RemoteCacheController.
 */


namespace Drupal\tbe_remote_cache_purger\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;

use Symfony\Component\HttpFoundation\Response;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class RemoteCacheController extends ControllerBase
{
  const MODULE_NAME = "tbe_remote_cache_purger";

  const DRUPAL_PURGE_ENDPOINT = "/api/drupal_cache_purge";
  const WORDPRESS_PURGE_ENDPOINT = "/?tbe=clear";
  const VARNISH_PURGE_ENDPOINT = "/api/varnish_cache_purge";
  const HASH_ALGORITHM_DEFAULT = "sha256";

  public function purgeCache($cacheType, $nid): Response
  {
    // Get config
    $config = \Drupal::config('tbe_remote_cache_purger.settings');
    $salt = $config->get('hash_salt');

    // Load the node from id passed
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->load($nid);

    // Build calling environment URL
    $url = $node->get("field_site_url");

    // Check for shield values
    $shieldCredentials = '';
    if ($node->get('field_site_security')->value) {
      $shieldCredentials = $node->get('field_shield_username')->getString();
      $shieldCredentials .= ':' . $node->get('field_shield_password')->getString() . '@';
    }

    foreach ($url as $item) {
      $search = '://';
      $pos = strpos($item->getString(), $search);
      // format url with shield credentials
      $siteUrl = substr_replace($item->getString(), $shieldCredentials, $pos + strlen($search), 0);

      // Call the services for Drupal, Varnish or CloudFlare
      if ($cacheType === "drupal") {
        $cacheResponse = $this->purgeDrupalCache(
          $node->get("field_purger_api_key")->getString(),
          $salt,
          $siteUrl
        );
        return $this->showPurgeResponse(
          $cacheResponse,
          "Drupal cache was successfully cleared",
          "Unable to purge Drupal cache!"
        );
      } elseif ($cacheType === "wordpress") {
        $cacheResponse = $this->purgeWordpressCache(
          $node->get("field_purger_api_key")->getString(),
          $salt,
          $siteUrl
        );
        return $this->showPurgeResponse(
          $cacheResponse,
          "Wordpress cache purged!",
          "Unable to purge Wordpress cache!"
        );
      } elseif ($cacheType === "varnish") {
        $cacheResponse = $this->purgeVarnishCache(
          $node->get("field_purger_api_key")->getString(),
          $salt,
          $siteUrl
        );
        return $this->showPurgeResponse(
          $cacheResponse,
          "Varnish cache purged!",
          "Unable to purge Varnish cache!"
        );
      } elseif ($cacheType === "cloudflare") {
        $host = parse_url($item->getString());
        $cacheResponse = $this->purgeCloudflareCache($nid, $host['host']);

        return $this->showPurgeResponse(
          $cacheResponse,
          "Cloudflare cache was successfully cleared",
          "Unable to purge Cloudflare caches"
        );
      } elseif ($cacheType === "incapsula") {
        // Stub for now.  We can easily add proper Incapsula handling if/when needed.
        $cacheResponse = $this->purgeIncapsulaCache();

        return $this->showPurgeResponse(
          $cacheResponse,
          "Incapsula cache purged!",
          "Unable to purge Incapsula cache!"
        );
      }
    }

    $this->getLogger(self::MODULE_NAME)->error("Unsupported cache type! $cacheType");

    return new AjaxResponse(null, 500);
  }

  private function purgeDrupalCache(string $apiToken, string $apiSalt, string $siteUrl): GuzzleResponse
  {
    if (!empty($apiToken) && !empty($apiSalt)) {
      $hashedToken = $this->getHashedToken($apiToken, $apiSalt);
      try {
        return (new Client())->request(
          "POST",
          $siteUrl . self::DRUPAL_PURGE_ENDPOINT,
          [
            "verify" => Settings::get("tbe_purge_verify_ssl"),
            "headers" =>
              [
                "content-type" => "application/json",
                "token" => "$hashedToken"
              ]
          ]
        );
      } catch (GuzzleException $guzzleException) {
        $this->getLogger(self::MODULE_NAME)->error("Failed attempt to purge Drupal cache. " .
          $guzzleException->getMessage());
      }
    }

    return new GuzzleResponse(500);
  }

  private function purgeWordpressCache(string $apiToken, string $apiSalt, string $siteUrl): GuzzleResponse
  {
    if (!empty($apiToken) && !empty($apiSalt)) {
      $hashedToken = $this->getHashedToken($apiToken, $apiSalt);
      try {
        return (new Client())->request(
          "POST",
          $siteUrl . self::WORDPRESS_PURGE_ENDPOINT,
          [
            "verify" => Settings::get("tbe_purge_verify_ssl"),
            "headers" =>
              [
                "content-type" => "application/json",
                "token" => "$hashedToken"
              ]
          ]
        );
      } catch (GuzzleException $guzzleException) {
        $this->getLogger(self::MODULE_NAME)->error("Failed attempt to purge Wordpress cache. " .
          $guzzleException->getMessage());
      }
    }

    return new GuzzleResponse(500);
  }

  private function purgeVarnishCache(string $apiToken, string $apiSalt, string $siteUrl): GuzzleResponse
  {
    if (!empty($apiToken) && !empty($apiSalt) && !empty($siteUrl)) {
      $hashedToken = $this->getHashedToken($apiToken, $apiSalt);

      try {
        $this->getLogger(self::MODULE_NAME)->debug("About to attempt Varnish purge.");
        return (new Client())->request(
          "POST",
          $siteUrl . self::VARNISH_PURGE_ENDPOINT,
          [
            "verify" => Settings::get("tbe_purge_verify_ssl"),
            "headers" =>
              [
                "content-type" => "application/json",
                "token" => "$hashedToken",
              ]
          ]
        );
      } catch (GuzzleException $guzzleException) {
        $this->getLogger(self::MODULE_NAME)->error("Failed attempt to purge Varnish cache. " .
          $guzzleException->getTraceAsString());
      }
    }

    return new GuzzleResponse(500);
  }

  private function purgeCloudflareCache(string $nid, string $hostName): GuzzleResponse
  {
    $config = \Drupal::config('tbe_remote_cache_purger.settings');
    // Load the node from id passed
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->load($nid);

    $cloudflareApiKey = $node->get("field_waf_api_key")->getString();
    $cloudflareEmail = $node->get("field_waf_login_email")->getString();
    $cloudflareZone = $node->get("field_waf_zone_id")->getString();
    $cloudflareLicenseType = strtolower($node->get("field_waf_license_level")->getString());
    \Drupal::logger("cf")->info($cloudflareApiKey);
    \Drupal::logger("cf")->info($cloudflareEmail);
    \Drupal::logger("cf")->info($cloudflareZone);
    $domain = $this->getCloudflareDomain($cloudflareApiKey, $cloudflareEmail, $cloudflareZone);
    $logger = $this->getLogger(self::MODULE_NAME);

    if (!empty($cloudflareApiKey) && !empty($cloudflareEmail) && !empty($cloudflareZone) && !empty($domain)) {
      try {
        $logger->debug("About to attempt Cloudflare purge.");
        $purge = ['"purge_everything"' => true];
        if ($cloudflareLicenseType === 'enterprise') {
          $response = (new Client())->request(
            "POST",
            "https://api.cloudflare.com/client/v4/zones/$cloudflareZone/purge_cache",
            [
              "verify" => Settings::get("tbe_purge_verify_ssl"),
              "headers" =>
                [
                  "X-Auth-Email" => $cloudflareEmail,
                  "X-Auth-Key" => $cloudflareApiKey,
                  "Content-Type" => "application/json",
                ],
              "body" =>
                json_encode([
                  "hosts" => [$hostName],
                ])
            ]
          );
        }
        elseif($cloudflareLicenseType === 'business'){
        $response = (new Client())->request(
          "POST",
          "https://api.cloudflare.com/client/v4/zones/$cloudflareZone/purge_cache",
          [
            "verify" => Settings::get("tbe_purge_verify_ssl"),
            "headers" =>
              [
                "X-Auth-Email" => $cloudflareEmail,
                "X-Auth-Key" => $cloudflareApiKey,
                "Content-Type" => "application/json",
              ],
            "body" =>
              json_encode([
                "purge_everything" => TRUE,
              ])
          ]
        );
      }

        if ($response->getStatusCode() === 200) {
          $logger->info("Cloudflare Cache successfully purged! Response: " . $response->getBody());
          return new GuzzleResponse($response->getStatusCode());
        } else {
          $logger->warning("Failed attempt to purge Cloudflare Cache. Response: " . $response->getBody());
        }
      } catch (GuzzleException $guzzleException) {
        $this->getLogger(self::MODULE_NAME)->error("Failed attempt to purge Cloudflare cache. " .
          $guzzleException->getMessage());
      }
    }

    return new GuzzleResponse(500, [], "Some data missing from request.");
  }

  private function purgeIncapsulaCache(): GuzzleResponse
  {
    // Just a place-holder until we need it.
    return new GuzzleResponse(200);
  }

  private function showPurgeResponse(
    GuzzleResponse $response,
    string $successMessage,
    string $failMessage
  ): AjaxResponse
  {
    $ajaxResponse = new AjaxResponse();

    if ($response->getStatusCode() == 200) {
      $this->messenger()->addStatus($successMessage, true);
    } else {
      $this->messenger()->addWarning($this->t($failMessage));
    }

    return $ajaxResponse;
  }

  private function getHashedToken(string $apiToken, string $apiSalt): string
  {
    return hash_hmac(
      Settings::get("tbe_purge_hash_algorithm") ?? self::HASH_ALGORITHM_DEFAULT,
      $apiToken,
      date("Y-m-d") . $apiSalt
    );
  }

  private function getCloudflareDomain(string $apiKey, string $authEmail, string $zone): string
  {
    $logger = $this->getLogger(self::MODULE_NAME);

    try {
      $logger->debug("Attempting to GET Zone info.");
      $response = (new Client())->request(
        "GET",
        "https://api.cloudflare.com/client/v4/zones/$zone",
        [
          "verify" => Settings::get("tbe_purge_verify_ssl"),
          "headers" =>
            [
              "X-Auth-Email" => $authEmail,
              "X-Auth-Key" => $apiKey,
              "Content-Type" => "application/json",
            ],
        ]
      );

      if ($response->getStatusCode() === 200) {
        $jsonResponse = $response->getBody();

        $logger->info("Zone info received! Response: $jsonResponse");

        $jsonResponse = json_decode($jsonResponse, true);

        $logger->info("Domain: " . $jsonResponse["result"]["name"]);

        return $jsonResponse["result"]["name"];
      } else {
        $logger->warning("Failed attempt to GET Cloudflare info. Response: " . $response->getBody());
      }
    } catch (GuzzleException $guzzleException) {
      $this->getLogger(self::MODULE_NAME)->error("Failed attempt to purge Cloudflare cache. " .
        $guzzleException->getMessage());
    }

    return "";
  }
}
