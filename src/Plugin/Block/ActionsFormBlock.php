<?php
/**
 * @file
 * Contains \Drupal\tbe_dashboard\Plugin\Block\DeployBranchBlock.
 */

namespace Drupal\tbe_remote_cache_purger\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;

/**
 * Provides a 'Branch Update Form' block.
 *
 * @Block(
 *   id = "cache_actions_form_block",
 *   admin_label = @Translation("Cache Purge Form"),
 *   category = @Translation("Dashboard")
 * )
 */
class ActionsFormBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

        $form = \Drupal::formBuilder()->getForm('Drupal\tbe_remote_cache_purger\Form\CacheActionsForm');

        return $form;
    }
}