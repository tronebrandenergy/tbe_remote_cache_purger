<?php

namespace Drupal\tbe_remote_cache_purger\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Form for mccd_semesters settings.
 */
class ConfigForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'tbe_remote_cache_purger_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->getConfig();

        $form['hash_salt'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Hash Salt for Purger'),
            '#default_value' => $config->get('hash_salt'),
            '#required' => true,
        ];

        return parent::buildForm($form, $form_state);
    }
}
