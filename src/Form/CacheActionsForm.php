<?php

namespace Drupal\tbe_remote_cache_purger\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tbe_remote_cache_purger\Controller\RemoteCacheController;

/**
 * Class Branch Update Form.
 */
class CacheActionsForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['tbe_remote_cache_purge.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'cache_actions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $node = \Drupal::routeMatch()->getParameter('node');
    if (isset($node)) {
      $form['#cache'] = ['max-age' => 0];

      $form['actions'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Cache Actions')
      ];

      $form['actions']['all'] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear All Caches'),
        '#attributes' =>
          [
            'onclick' => 'if(!confirm("Are you sure you want to clear ALL caches?")){return false;}'
          ],
      ];
      
      // platform is defaulted to 'drupal' if not set
      $platform = !empty($node->get('field_platform')->getString()) ? strtolower($node->get('field_platform')->entity->getName()) : 'drupal';
      $form['actions'][$platform] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear ' . ucfirst($platform) . ' Cache'),
        '#submit' => ['::clear' . ucfirst($platform) . 'Caches'],
        '#attributes' =>
          [
            'onclick' => 'if(!confirm("Are you sure you want to clear ' . strtoupper($platform) . ' caches?")){return false;}'
          ],
      ];
        

      if (isset($node->get('field_varnish')->value)) {
        if ($node->get('field_varnish')->value) {
          $form['actions']['varnish'] = [
            '#type' => 'submit',
            '#value' => $this->t('Clear Varnish Caches'),
            '#submit' => ['::clearVarnishCaches'],
            '#attributes' =>
              [
                'onclick' => 'if(!confirm("Are you sure you want to clear VARNISH caches?")){return false;}'
              ],
          ];
        }
      }
      if (!empty($node->get('field_waf_type')->getString())) {
        $wafProvider = strtolower($node->get('field_waf_type')->entity->getName());

        if ($wafProvider === "cloudflare") {
          $form['actions']['cloudflare'] = [
            '#type' => 'submit',
            '#value' => $this->t('Clear Cloudflare Caches'),
            '#submit' => ['::clearCloudflareCaches'],
            '#attributes' =>
              [
                'onclick' => 'if(!confirm("Are you sure you want to clear CLOUDFLARE caches?")){return false;}'
              ],
          ];
        } elseif ($wafProvider === "incapsula") {
          $form['actions']['incapsula'] = [
            '#type' => 'submit',
            '#value' => $this->t('Clear Incapsula Caches'),
            '#submit' => ['::clearIncapsulaCaches'],
            '#attributes' =>
              [
                'onclick' => 'if(!confirm("Are you sure you want to clear INCAPSULA caches?")){return false;}'
              ],
          ];
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $node = \Drupal::routeMatch()->getParameter('node');
    $platform = !empty($node->get('field_platform')->getString()) ? strtolower($node->get('field_platform')->entity->getName()) : 'drupal';

    if ($platform === 'drupal') $this->clearDrupalCaches();
    if ($platform === 'wordpress') $this->clearWordpressCaches();

    if (isset($node->get('field_varnish')->value)) {
      if ($node->get('field_varnish')->value) {
        $this->clearVarnishCaches();
      }
    }

    if (!empty($node->get('field_waf_type')->getString())) {
      $wafProvider = strtolower($node->get('field_waf_type')->entity->getName());
      if ($wafProvider === "cloudflare") {
        $this->clearCloudflareCaches();
      } elseif ($wafProvider === "incapsula") {
        $this->clearIncapsulaCaches();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearCaches($cacheType)
  {
    $controller = new RemoteCacheController();
    $node = \Drupal::routeMatch()->getParameter('node');
    $controller->purgeCache($cacheType, $node->id());
  }

  /**
   * {@inheritdoc}
   */
  public function clearDrupalCaches()
  {
    $this->clearCaches('drupal');
  }

  /**
   * {@inheritdoc}
   */
  public function clearWordpressCaches()
  {
    $this->clearCaches('wordpress');
  }

  /**
   * {@inheritdoc}
   */
  public function clearVarnishCaches()
  {
    $this->clearCaches('varnish');
  }

  /**
   * {@inheritdoc}
   */
  public function clearCloudflareCaches()
  {
    $this->clearCaches('cloudflare');
  }

  /**
   * {@inheritdoc}
   */
  public function clearIncapsulaCaches()
  {
    $this->clearCaches('incapsula');
  }

  public function get_waf_provider_from_id($tid)
  {
    if (isset($tid)) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($tid);
      return $term->getName();
    } else {
      return '';
    }
  }
}
